//
//  ContentView.swift
//  Coba
//
//  Created by Pijar Dwi Kusuma on 02/12/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView(){
            VStack {
                Image(systemName: "globe")
                    .imageScale(.large)
                    .foregroundColor(.accentColor)
                Text("Hello, world!")
            }
            .padding()
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
