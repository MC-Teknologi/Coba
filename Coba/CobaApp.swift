//
//  CobaApp.swift
//  Coba
//
//  Created by Pijar Dwi Kusuma on 02/12/22.
//

import SwiftUI

@main
struct CobaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
